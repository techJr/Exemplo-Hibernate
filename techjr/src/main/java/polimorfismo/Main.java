package polimorfismo;

public class Main {

	public static void main(String[] args) {
		Carro carro = new Palio();
		
		carro.acelerar();
		
		metodo1("JR", "Pereira");
		
	}
	
	
	public static void metodo1(String nome){
		System.out.println("METODO 1 " + nome);
	}
	
	public static void metodo1(String nome, String sobrenome){
		System.out.println("METODO 2 " + nome);
	}
}
