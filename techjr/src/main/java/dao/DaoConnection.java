package dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import model.Filho;
import model.Mae;
import model.Pessoa;

public class DaoConnection {

	public static void main(String[] args) {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("techjrWorkshop");
		EntityManager em = emf.createEntityManager();
		//
		Pessoa pessoa = new Pessoa();
		pessoa.setNome("Ed");
		pessoa.setSobrenome("Ed");

		em.getTransaction().begin();

		em.persist(pessoa);
		pessoa.setIdade("1");
		em.persist(pessoa);

		Mae mae = new Mae();
		mae.setNome("Dona maria");

		em.persist(mae);

		Filho filho1 = new Filho();
		Filho filho2 = new Filho();

		List<Filho> filhos = new ArrayList<Filho>();
		filhos.add(filho1);
		filhos.add(filho2);

		filho1.setNome("Juquinha");
		filho1.setMae(mae);
		filho2.setNome("Claudinho");
		filho2.setMae(mae);

		em.persist(filho1);
		em.persist(filho2);

		em.getTransaction().commit();

		emf.close();

	}

}
